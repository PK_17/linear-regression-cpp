#include "Trainee.h"

Trainee::Trainee(Description& readyDescription, std::vector<double>& readyInput)
{
	m_Out = calculateOut(readyDescription, readyInput);
}

std::vector<double> Trainee::calculateOut(Description& readyDescription, std::vector<double>& readyInput) // TODO: only degrees or degrees + splits???
{
	std::vector<double> out;
	double result;
	double tmpResult;
	int descLineCount = (int)readyDescription.variableIndices.size() / readyDescription.degree;
	int inLineCount = (int)readyInput.size() / readyDescription.dimensions;
	//std::cout << "Trainee - inLineCount = readyInput.size() / readyDescription.dimensions  ----> " << inLineCount << "\n";
	int index;

	for (int res = 0; res < inLineCount; res++) {
		result = 0;
		for (int row = 0; row < descLineCount; row++) {
			tmpResult = 1;
			for (int col = 0; col < readyDescription.degree; col++) {
				index = readyDescription.variableIndices[col + row * readyDescription.degree] - 1;
				if (index == -1) continue;
				tmpResult *= readyInput[index + res * readyDescription.dimensions];
			}
			result += tmpResult * readyDescription.weights[row];
		}
		out.push_back(result);
	}

	return out;
}

void Trainee::printOut()
{
	int lineCount = (int)m_Out.size();

	for (int i = 0; i < lineCount; i++) {
		std::cout << m_Out[i] << "\n";
	}
}