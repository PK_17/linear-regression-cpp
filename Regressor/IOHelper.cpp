#include "IOHelper.h"

IOHelper::IOHelper(int argc, char* argv[])
{
	for (int i = 0; i < argc; i++) {
		if (strcmp(argv[i], "-t") == 0) {
			m_MainSet = readTrainSet(argv[++i]);
		}
	}	
	m_Input = readIn();
}

Set IOHelper::readTrainSet(char* filePath)
{
	std::fstream file(filePath);
	std::stringstream ss;
	std::string line;
	std::vector<double> set;
	int dimensions;
	int lineCount = 0, numberCount = 0;

	if (file.fail()) {
		std::cout << "File " << filePath << " could not be opened!\n";
	}
	else {
		while (getline(file, line)) {
			if (line.empty()) break;
			ss << line << " ";
			lineCount++;
		}

		while (getline(ss, line, ' ')) {
			set.push_back(std::stod(line));
			numberCount++;
		}

		dimensions = (numberCount / lineCount) - 1;
	}

	return{ set, dimensions };
}

std::vector<double> IOHelper::readIn()
{
	std::stringstream ss;
	std::string line;
	std::vector<double> in;

	while (getline(std::cin, line)) {
		if (line.empty()) break;
		ss << line << " ";
	}

	while (getline(ss, line, ' ')) {
		in.push_back(std::stod(line));
	}

	return in;
}

void IOHelper::printOut(std::vector<double>& out) const
{
	int outCount = (int)out.size();

	for (int i = 0; i < outCount; i++) {
		std::cout << out[i] << "\n";
	}
}