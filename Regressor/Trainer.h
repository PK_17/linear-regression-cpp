#pragma once
#include <cmath>
#include "IOHelper.h"
#include "Converter.h"

#define ITERATIONS 1000
#define ALPHA 0.08
#define TOLERANCE 0.0000001
#define INIT 1000
#define FACTOR 2

class Trainer 
{
public:
	Description m_ReadyDescription;

	void train(ParsedSet& parsedSet, Description& inDescription, int iterations, double trainingStep, double tolerance);
	ParsedSet parseSet(Set& set);

	// Testing
	void printReadyDescription();
};