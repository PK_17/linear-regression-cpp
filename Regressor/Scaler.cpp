#include "Scaler.h"

Scaler::Scaler(ParsedSet& set, std::vector<double>& in)
{
	findMaxMin(set, in);
}

void Scaler::findMaxMin(ParsedSet& set, std::vector<double>& in)
{
	// Add input vector to the training set's input
	int inputCount = (int)in.size();

	for (int i = 0; i < inputCount; i++) {
		set.input.push_back(in[i]);
	}

	std::vector<double> min;
	std::vector<double> max;
	int inCount = (int)set.input.size() / set.dimensions;
	int exOutCount = (int)set.expectedOutput.size();

	for (int i = 0; i < set.dimensions; i++) {
		max.push_back(set.input[i]);
		min.push_back(set.input[i]);
	}

	max.push_back(set.expectedOutput[0]);
	min.push_back(set.expectedOutput[0]);

	for (int col = 0; col < set.dimensions + 1; col++) {
		for (int row = 1; row < inCount; row++) {
			if (col == set.dimensions && row < exOutCount) {
				if (max[col] < set.expectedOutput[row]) {
					max[col] = set.expectedOutput[row];
				}
				if (min[col] > set.expectedOutput[row]) {
					min[col] = set.expectedOutput[row];
				}
			}
			else if (col != set.dimensions){
				if (max[col] < set.input[col + row * set.dimensions]) {
					max[col] = set.input[col + row * set.dimensions];
				}
				if (min[col] > set.input[col + row * (set.dimensions)]) {
					min[col] = set.input[col + row * (set.dimensions)];
				}
			}
		}
	}

	m_Max = max;
	m_Min = min;
}

void Scaler::scaleSet(Set& set)
{
	int lineCount = (int)set.set.size() / (set.dimensions + 1);

	m_ScaledSet.dimensions = set.dimensions;
	for (int row = 0; row < lineCount; row++) {
		for (int col = 0; col < set.dimensions + 1; col++)
		{
			m_ScaledSet.set.push_back(SCALING_FACTOR * ((set.set[col + (set.dimensions + 1) * row] - m_Min[col]) / (m_Max[col] - m_Min[col])) - 1);
		}
	}
}

void Scaler::scaleInput(std::vector<double>& in, int dimensions)
{
	int lineCount = (int)in.size() / dimensions;

	for (int row = 0; row < lineCount; row++) {
		for (int col = 0; col < dimensions; col++)
		{
			m_ScaledInput.push_back(SCALING_FACTOR * ((in[col + row * dimensions] - m_Min[col]) / (m_Max[col] - m_Min[col])) - 1);
		}
	}
}

std::vector<double> Scaler::reScale(std::vector<double>& out)
{
	std::vector<double> reScaledOut;
	int lineCount = (int)out.size();
	int outIndex = m_ScaledSet.dimensions;

	for (int row = 0; row < lineCount; row++) {
		reScaledOut.push_back((1 / (double)SCALING_FACTOR) * (m_Max[outIndex] - m_Min[outIndex]) * (out[row] + 1) + m_Min[outIndex]);
	}

	return reScaledOut;
}

void Scaler::printMinMax()
{
	int colCount = (int)m_Max.size();

	std::cout << "Maximum values column-wise:\n";
	for (int i = 0; i < colCount; i++) {
		std::cout << m_Max[i] << " ";
	}
	std::cout << "\nMinimum values column-wise:\n";
	for (int i = 0; i < colCount; i++) {
		std::cout << m_Min[i] << " ";
	}
}

void Scaler::printScaledSet()
{
	int lineCount = (int)m_ScaledSet.set.size() / (m_ScaledSet.dimensions + 1);

	for (int row = 0; row < lineCount; row++) {
		for (int col = 0; col < m_ScaledSet.dimensions + 1; col++) {
			std::cout << m_ScaledSet.set[col + row * (m_ScaledSet.dimensions + 1)] << " ";
		}
		std::cout << "\n";
	}
}

void Scaler::printScaledInput(int dimensions)
{
	int lineCount = (int)m_ScaledInput.size() / dimensions;

	for (int row = 0; row < lineCount; row++) {
		for (int col = 0; col < dimensions; col++) {
			std::cout << m_ScaledInput[col + row * dimensions] << " ";
		}
		std::cout << "\n";
	}
}