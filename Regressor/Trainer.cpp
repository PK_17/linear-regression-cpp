#include "Trainer.h"

void Trainer::train(ParsedSet& parsedSet, Description& inDescription, int iterations, double trainingStep, double tolerance)
{
	double quality = INIT;
	double tmpValue;
	int weightSize = (int)inDescription.weights.size();
	int outputSize = (int)parsedSet.expectedOutput.size();
	std::vector<double> tmpGradient, gradient;
	int doneIterations = 0;

	while (quality > tolerance && doneIterations < iterations) {
		quality = 0;
		gradient.clear();
		for (int set = 0; set < outputSize; set++) {
			tmpValue = 0;
			// Calculate values of the function for current input (parsedSet)
			for (int weight = 0; weight < weightSize; weight++) {
				if (weight == weightSize - 1) {
					tmpValue += inDescription.weights[weight];
				}
				else {
					tmpValue += inDescription.weights[weight]
						* parsedSet.input[((parsedSet.dimensions - 1) - weight) + parsedSet.dimensions * set];
				}
			}

			tmpGradient.clear();
			// Calculate partial derivatives of the gradient
			for (int weight = 0; weight < weightSize; weight++) {
				if (weight == weightSize - 1) {
					tmpGradient.push_back(tmpValue - parsedSet.expectedOutput[set]);
				}
				else {
					tmpGradient.push_back((tmpValue - parsedSet.expectedOutput[set])
						* parsedSet.input[((parsedSet.dimensions - 1) - weight) + parsedSet.dimensions * set]);
				}
			}

			// First tmpGradient can be just copied. Each next should be added to the gradient.
			if (set == 0) {
				gradient = tmpGradient;
			}
			else {
				for (int weight = 0; weight < weightSize; weight++) {
					gradient[weight] += tmpGradient[weight];
				}
			}
		}

		// Update weights after evaluation of all lines in the parsedSet
		for (int weight = 0; weight < weightSize; weight++) {
			inDescription.weights[weight] -= trainingStep * ((1 / (double)outputSize) * gradient[weight]);
		}

		// Calculate gradient's vector length to assess its quality
		for (int weight = 0; weight < weightSize; weight++) {
			quality += gradient[weight] * gradient[weight];
		}

		quality = (1 / (FACTOR * (double)outputSize)) * sqrt(quality);

		doneIterations++;
	}

	m_ReadyDescription = inDescription;
}

ParsedSet Trainer::parseSet(Set& set)
{
	ParsedSet parsedSet;
	int lineCount = (int)set.set.size() / (set.dimensions + 1);

	parsedSet.dimensions = set.dimensions;
	for (int row = 0; row < lineCount; row++) {
		for (int col= 0; col < set.dimensions + 1; col++) {
			if (col == set.dimensions) {
				parsedSet.expectedOutput.push_back(set.set[col + (row * (set.dimensions + 1))]);
			}
			else {
				parsedSet.input.push_back(set.set[col + (row * (set.dimensions + 1))]);
			}
		}
	}

	return parsedSet;
}

void Trainer::printReadyDescription()
{
	int lineCount = (int)m_ReadyDescription.variableIndices.size() / m_ReadyDescription.degree;

	std::cout << m_ReadyDescription.dimensions << " " << m_ReadyDescription.degree << "\n";
	for (int row = 0; row < lineCount; row++) {
		for (int col = 0; col < m_ReadyDescription.degree; col++) {
			std::cout << m_ReadyDescription.variableIndices[col + row * m_ReadyDescription.degree] << " ";
		}
		std::cout << m_ReadyDescription.weights[row] << "\n";
	}
}