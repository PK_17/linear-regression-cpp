#pragma once
#include <vector>
#include <fstream>
#include <sstream>
#include <cstring>
#include <string>
#include <iostream>

struct Set {
	std::vector<double> set;
	int dimensions;
};

struct ParsedSet {
	std::vector<double> input;
	std::vector<double> expectedOutput;
	int dimensions;
};

class IOHelper
{
private:
	Set readTrainSet(char* filePath);
	std::vector<double> readIn();

public:
	Set m_MainSet;
	std::vector<double> m_Input;

	IOHelper(int argc, char* argv[]);
	void printOut(std::vector<double>& out) const;
};