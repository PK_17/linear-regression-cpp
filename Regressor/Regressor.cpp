#include "Converter.h"
#include "Scaler.h"
#include "Validator.h"

int main(int argc, char* argv[])
{
	int hyperparameter;
	Trainer trainer;
	ParsedSet parsedMainSetScaling;
	ParsedSet parsedMainSet;

	// Main training set, as well as input vector are sotred in the ioHelper object.
	IOHelper ioHelper(argc, argv);

	parsedMainSetScaling = trainer.parseSet(ioHelper.m_MainSet);
	Scaler scaler(parsedMainSetScaling, ioHelper.m_Input);
	scaler.scaleSet(ioHelper.m_MainSet);
	scaler.scaleInput(ioHelper.m_Input, ioHelper.m_MainSet.dimensions);
	
	Validator validator(scaler.m_ScaledSet, SPLITS);
	validator.createSplits();
	
	Converter converter(ioHelper.m_MainSet.dimensions);

	validator.createEvaluation(converter);
	hyperparameter = validator.selectDegree();
	std::cout << "Selected degree: " << hyperparameter << "\n\n";

	parsedMainSet = trainer.parseSet(scaler.m_ScaledSet);
	parsedMainSet.dimensions = converter.m_ParsedDescriptions[hyperparameter - 1].dimensions;
	
	converter.parseInput(parsedMainSet.input, hyperparameter - 1);
	parsedMainSet.input = converter.m_CurrentParsedInput;
	trainer.train(parsedMainSet, converter.m_ParsedDescriptions[hyperparameter - 1], ITERATIONS, ALPHA, TOLERANCE);
	converter.parseInput(scaler.m_ScaledInput, hyperparameter - 1);

	Trainee trainee(trainer.m_ReadyDescription, converter.m_CurrentParsedInput);
	std::vector<double> out = scaler.reScale(trainee.m_Out);
	ioHelper.printOut(out);
}
