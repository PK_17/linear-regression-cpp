#pragma once
#include <random>
#include "IOHelper.h"

#define DEGREE_COUNT 5
#define UPPER_BOUNDARY 1.0
#define LOWER_BOUNDARY -1.0

struct Description {
	int dimensions;
	int degree;
	std::vector<int> variableIndices;
	std::vector<double> weights;
};

struct NextLine {
	std::vector<int> line;
	int sum;
};

class Converter 
{
private:
	std::vector<Description> generateDescriptions(int dimensions);
	NextLine generateNextLine(std::vector<int>& line, int dimensions, int degree);
	std::vector<Description> parseDescriptions();

public:
	std::vector<Description> m_Descriptions;
	std::vector<Description> m_ParsedDescriptions;
	std::vector<double> m_CurrentParsedInput;

	Converter(int dimensions);
	void parseInput(std::vector<double>& input, int degree);

	// Testing
	void printDescriptions() const;
	void printParsedDescriptions() const;
	void printCurrentParsedInput(int degree) const;
};