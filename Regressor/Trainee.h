#pragma once
#include "IOHelper.h"
#include "Converter.h"
#include "Validator.h"

// TODO: Testing
class Trainee
{
private:
	std::vector<double> calculateOut(Description& readyDescriptions, std::vector<double>& readyInput);
public:
	std::vector<double> m_Out;

	Trainee(Description& readyDescription, std::vector<double>& readyInput);

	// Testing
	void printOut();
};