#include "Validator.h"

#define INIT 1000

Validator::Validator(Set& set, int splitsCount)
	: m_Splits(), m_Evaluation()
{
	m_Set = set;
	m_SplitsCount = splitsCount;
}

void Validator::createSplits()
{
	std::vector<Set> trainSets;
	std::vector<Set> validationSets;
	int colCount = m_Set.dimensions + 1;
	int lineCount = (int)m_Set.set.size() / colCount;
	int splitPoint = (int)std::floor((float)lineCount / (float)m_SplitsCount);
	int counter;
	Set trainPart;
	Set validationPart;

	trainPart.dimensions = m_Set.dimensions;
	validationPart.dimensions = m_Set.dimensions;
	for (int i = 0; i < m_SplitsCount; i++) {
		counter = 0;
		trainPart.set.clear();
		validationPart.set.clear();
		for (int row = 0; row < lineCount; row++) {
			for (int col = 0; col < colCount; col++) {
				if (row > (i * splitPoint - 1) && (counter < colCount * splitPoint)) {
					validationPart.set.push_back(m_Set.set[col + row * colCount]);
					counter++;
				}
				else {
					trainPart.set.push_back(m_Set.set[col + row * colCount]);
				}
			}
		}

		trainSets.push_back(trainPart);
		validationSets.push_back(validationPart);
	}

	m_Splits = { trainSets, validationSets };
}

double Validator::evaluate(Converter& converter, int split, int degree)
{
	Trainer trainer;
	Description readyDescription;
	ParsedSet parsedValidationSet;
	ParsedSet parsedTrainingSet;
	std::vector<double> out;
	double evaluation = 0;

	if (degree == 0) {
		//std::cout << "Degree == 1\n";
		parsedTrainingSet = trainer.parseSet(m_Splits.trainSets[split]);
		trainer.train(parsedTrainingSet, converter.m_Descriptions[degree], ITERATIONS, ALPHA, TOLERANCE);
		readyDescription = trainer.m_ReadyDescription;
		parsedValidationSet = trainer.parseSet(m_Splits.validationSets[split]);
	}
	else {
		//std::cout << "Degree == " << degree + 1 << "\n";
		parsedTrainingSet = trainer.parseSet(m_Splits.trainSets[split]);
		parsedTrainingSet.dimensions = converter.m_ParsedDescriptions[degree].dimensions;
		converter.parseInput(parsedTrainingSet.input, degree);
		parsedTrainingSet.input = converter.m_CurrentParsedInput;
		//std::cout << "Parsed training set input:\n";
		//converter.printCurrentParsedInput(degree);
		//std::cout << "\n";

		trainer.train(parsedTrainingSet, converter.m_ParsedDescriptions[degree], ITERATIONS, ALPHA, TOLERANCE);
		readyDescription = trainer.m_ReadyDescription;
		parsedValidationSet = trainer.parseSet(m_Splits.validationSets[split]);
		
		parsedValidationSet.dimensions = converter.m_Descriptions[degree].dimensions;
		converter.parseInput(parsedValidationSet.input, degree);
		parsedValidationSet.input = converter.m_CurrentParsedInput;
		//std::cout << "Parsed validation set input:\n";
		//converter.printCurrentParsedInput(degree);
		//std::cout << "\n";
	}

	Trainee trainee(readyDescription, parsedValidationSet.input);
	int outCount = (int)trainee.m_Out.size();

	for (int i = 0; i < outCount; i++) {
		evaluation += (parsedValidationSet.expectedOutput[i] - trainee.m_Out[i]) 
			* (parsedValidationSet.expectedOutput[i] - trainee.m_Out[i]);
	}
	//std::cout << "\n\n";
	return evaluation / (double)outCount;
}

void Validator::createEvaluation(Converter& converter)
{
	EvaluationData evaluationData;

	for (int degree = 0; degree < DEGREE_COUNT; degree++) {
		evaluationData.degree.push_back(degree + 1);
		for (int split = 0; split < SPLITS; split++) {
			evaluationData.evalValidation.push_back(evaluate(converter, split, degree));
		}
	}

	m_Evaluation = evaluationData;
}

int Validator::selectDegree()
{
	int lineCount = (int)m_Evaluation.degree.size();
	double smallestSum = INIT;
	double tmpSum;
	int index;

	for (int row = 0; row < lineCount; row++) {
		tmpSum = 0;
		for (int col = 0; col < SPLITS; col++) {
			tmpSum += m_Evaluation.evalValidation[col + row * SPLITS];
		}
		if (tmpSum < smallestSum) {
			smallestSum = tmpSum;
			index = row;
		}
	}

	return m_Evaluation.degree[index];
}

void Validator::printSplits() const
{
	std::cout << "Dimensions -> TrainSet: " << m_Splits.trainSets[0].dimensions 
		<< "; ValidationSet: " << m_Splits.validationSets[0].dimensions << "\n\n";

	for (int i = 0; i < m_SplitsCount; i++) {
		int colCount = (int)m_Splits.trainSets[i].dimensions + 1;
		int trainLineCount = (int)(m_Splits.trainSets[i].set.size() / colCount);
		int validationLineCount = (int)(m_Splits.validationSets[i].set.size() / colCount);
		
		std::cout << "TrainSet[" << i << "]:" << "\n";
		for (int row = 0; row < trainLineCount; row++) {
			for (int col = 0; col < colCount; col++) {
				std::cout << m_Splits.trainSets[i].set[col + row * colCount] << " ";
			}
			std::cout << "\n";
		}

		std::cout << "ValidationSet[" << i << "]:" << "\n";
		for (int row = 0; row < validationLineCount; row++) {
			for (int col = 0; col < colCount; col++) {
				std::cout << m_Splits.validationSets[i].set[col + row * colCount] << " ";
			}
			std::cout << "\n";
		}

		std::cout << "\n";
	}
}

void Validator::printEvaluation() const
{
	int lineCount = (int)m_Evaluation.degree.size();
	int colCount = SPLITS;

	for (int row = 0; row < lineCount; row++) {
		std::cout << m_Evaluation.degree[row] << " ";
		for (int col = 0; col < colCount; col++) {
			std::cout << m_Evaluation.evalValidation[col + row * colCount] << " ";
		}
		std::cout << "\n";
	}
	std::cout << "\n";
}