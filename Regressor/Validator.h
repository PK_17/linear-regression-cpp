#pragma once
#include "IOHelper.h"
#include "Converter.h"
#include "Trainer.h"
#include "Trainee.h"

#define SPLITS 5

struct Splits {
	std::vector<Set> trainSets;
	std::vector<Set> validationSets;
};

struct EvaluationData {
	std::vector<int> degree;
	std::vector<double> evalValidation;
};

class Validator
{
private:
	Set m_Set;
	int m_SplitsCount;
	EvaluationData m_Evaluation;

	double evaluate(Converter& converter, int split, int degree);
	
public:
	Splits m_Splits;

	Validator(Set& set, int splitsCount);
	void createSplits();
	void createEvaluation(Converter& converter);
	int selectDegree();

	// Testing
	void printSplits() const;
	void printEvaluation() const;
};