#pragma once
#include "IOHelper.h"

#define SCALING_FACTOR 2

class Scaler
{
private:
	std::vector<double> m_Min;
	std::vector<double> m_Max;

	void findMaxMin(ParsedSet& set, std::vector<double>& in);
	

public:
	Set m_ScaledSet;
	std::vector<double> m_ScaledInput;

	Scaler(ParsedSet& set, std::vector<double>& in);
	void scaleSet(Set& set);
	void scaleInput(std::vector<double>& in, int dimensions);
	std::vector<double> reScale(std::vector<double>& out);

	// Testing
	void printMinMax();
	void printScaledSet();
	void printScaledInput(int dimensions);
};