#include "Converter.h"

Converter::Converter(int dimensions)
	: m_CurrentParsedInput(0)
{
	m_Descriptions = generateDescriptions(dimensions);
	m_ParsedDescriptions = parseDescriptions();
}

std::vector<Description> Converter::generateDescriptions(int dimensions)
{
	std::default_random_engine generator;
	std::uniform_real_distribution<double> distribution(LOWER_BOUNDARY, UPPER_BOUNDARY);
	std::vector<Description> descriptions;
	Description currentDescription;
	std::vector<int> currentLine;
	NextLine nextLine;

	currentDescription.dimensions = dimensions;
	
	for (int i = 0; i < DEGREE_COUNT; i++) {
		currentDescription.variableIndices.clear();
		currentDescription.weights.clear();
		currentDescription.degree = i + 1;
		currentLine.clear();
		// Initialize first line
		for (int i = 0; i < currentDescription.degree; i++) {
			currentLine.push_back(dimensions);
			currentDescription.variableIndices.push_back(currentLine[i]);
			currentDescription.weights.push_back(distribution(generator));
		}

		nextLine.sum = currentDescription.degree;
		while (nextLine.sum != 0) {
			nextLine = generateNextLine(currentLine, currentDescription.dimensions, currentDescription.degree);
			if (nextLine.sum > 0) {
				for (int j = 0; j < currentDescription.degree; j++) {
					currentDescription.variableIndices.push_back(nextLine.line[j]);
				}
				currentDescription.weights.push_back(distribution(generator));
			}
		}

		descriptions.push_back(currentDescription);
	}

	return descriptions;
}

NextLine Converter::generateNextLine(std::vector<int>& line, int dimension, int degree)
{
	int sum = 0;
	int lastNonZeroValue;
	int index;

	for (int i = degree - 1; i >= 0; i--) {
		sum += line[i];
	}

	if (sum == 0) return{ line, sum };

	for (int i = degree - 1; i >= 0; i--) {
		if (line[i] != 0) {
			lastNonZeroValue = line[i];
			index = i;
			break;
		}
	}

	if (index == degree - 1) {
		line[index] -= 1;
		return{ line, sum };
	}
	else if (index != degree - 1) {
		line[index] -= 1;
		for (int i = index + 1; i < degree; i++) {
			line[i] = lastNonZeroValue - 1;
		}
	}

	return{ line, sum };
}

std::vector<Description> Converter::parseDescriptions()
{
	std::vector<Description> parsedDescriptions;
	int firstNonLinearIndex = 1;

	parsedDescriptions.push_back(m_Descriptions[0]);
	for (int i = firstNonLinearIndex; i < DEGREE_COUNT; i++) {
		Description currentParsedDescription;
		int descLineCount = (int)m_Descriptions[i].variableIndices.size() / m_Descriptions[i].degree;

		currentParsedDescription.dimensions = descLineCount - 1;
		currentParsedDescription.degree = 1;

		for (int row = 0; row < descLineCount; row++) {
			currentParsedDescription.variableIndices.push_back(descLineCount - row - 1);
			currentParsedDescription.weights.push_back(m_Descriptions[i].weights[row]);
		}

		parsedDescriptions.push_back(currentParsedDescription);
	}

	return parsedDescriptions;
}

// TODO: Testing
void Converter::parseInput(std::vector<double>& input, int degree)
{
	std::vector<double> currentParsedInput;
	int inputRows = (int)input.size() / (m_Descriptions[degree].dimensions);
	int descLineCount = ((int)m_Descriptions[degree].variableIndices.size() / m_Descriptions[degree].degree) - 1; // Omit last line (no variables there)
	int index;
	double tmpResult;
	int inputRow = 0;
	int descLine = 0;
	
	while (inputRow < inputRows) {
		tmpResult = 1;
		for (int j = 0; j < m_Descriptions[degree].degree; j++) {
			index = m_Descriptions[degree].variableIndices[j + descLine * m_Descriptions[degree].degree] - 1;
			if (index == -1) continue;
			tmpResult *= input[index + inputRow * m_Descriptions[degree].dimensions];
		}

		if (descLine == descLineCount) {
			descLine = 0;
			inputRow++;
		}
		else {
			currentParsedInput.push_back(tmpResult);
			descLine++;
		}
	}

	m_CurrentParsedInput = currentParsedInput;
}

void Converter::printDescriptions() const 
{
	std::cout << "Number of descriptions: " << DEGREE_COUNT << "\n";
	for (int i = 0; i < DEGREE_COUNT; i++) {
		int lineCount = (int)m_Descriptions[i].variableIndices.size() / m_Descriptions[i].degree;

		std::cout << "\nDescription " << i + 1 << "\n";
		std::cout << m_Descriptions[i].dimensions << " " << m_Descriptions[i].degree << "\n";
		for (int row = 0; row < lineCount; row++) {
			for (int col = 0; col < m_Descriptions[i].degree; col++) {
				std::cout << m_Descriptions[i].variableIndices[col + row * m_Descriptions[i].degree] << " ";
			}
			std::cout << m_Descriptions[i].weights[row] << "\n";
		}
	}
}

void Converter::printParsedDescriptions() const
{
	int firstNonLinearIndex = 1;

	std::cout << "Number of parsed descriptions: " << DEGREE_COUNT << "\n";
	for (int i = 0; i < DEGREE_COUNT; i++) {
		int lineCount = (int)m_ParsedDescriptions[i].variableIndices.size();

		std::cout << "\nParsed description " << i + 1 << "\n";
		std::cout << m_ParsedDescriptions[i].dimensions << " " << m_ParsedDescriptions[i].degree << "\n";
		for (int row = 0; row < lineCount; row++) {
			for (int col = 0; col < m_ParsedDescriptions[i].degree; col++) {
				std::cout << m_ParsedDescriptions[i].variableIndices[col + row * m_ParsedDescriptions[i].degree] << " ";
			}
			std::cout << m_ParsedDescriptions[i].weights[row] << "\n";
		}
	}
}

void Converter::printCurrentParsedInput(int degree) const
{
	std::cout << "Data for printing...\n";
	int lineCount = (int)m_CurrentParsedInput.size() / (m_ParsedDescriptions[degree].dimensions);
	int colCount = m_ParsedDescriptions[degree].dimensions;
	std::cout << "lineCout = " << lineCount << "; colCount = " << colCount << "\n";
	for (int row = 0; row < lineCount; row++) {
		for (int col = 0; col < colCount; col++) {
			std::cout << m_CurrentParsedInput[col + row * colCount] << " ";
		}
		std::cout << "\n";
	}
}