## Description

This program performs linear regression on a given dataset in n-dimensional space. It selects the degree of the polynomial using cross-validation to fit the given data in the optimal way.

## Requirements

There are two .txt files required for the program to work properly - in.txt and set.txt.

# set.txt

Training set for the model. Each column is a vector representing all dimension variables (features) and expected value of the polynomial (in the last column). All values are separated by space and a dot is used as decimal separator. The file ends with one empty line.

Example:

x1 x2 y<br>
1.0 2.0 -12.34<br>
3.0 1.5 -9.55<br>
4.0 1.0	-5.03<br>
// empty line

The above is a representation of 2D dataset with expected values (y).

The general formula is as follows:

x1 x2 x3 x4 ... xn y<br>
x1 x2 x3 x4 ... xn y<br>
.<br>
.<br>
.<br>
x1 x2 x3 x4 ... xn y // last vector<br>
// empty line

# in.txt

This file provides an input for the trained model. Its format is almost identical to set.txt, but it lacks the last column (because we do not know the exact value for those combination of features' values).

Example:

x1 x2<br>
10.0 12.0<br>
-1.0 -3.0<br>
2.3 22.1<br>
// empty line

The general formula:

x1 x2 x3 x4 ... xn<br>
x1 x2 x3 x4 ... xn<br>
.<br>
.<br>
.<br>
x1 x2 x3 x4 ... xn// last vector<br>
// empty line

# Output

The porgram produces a file out.txt containing predictions for all vectors from in.txt as well as information about the selected degree.
Example:

Selected degree: 4

12.01<br>
2.03<br>
-3.90<br>
etc.<br>
